package com.techu.backend.model;


public class ReservaFechaModel {
    private String id;
    private String fecha;

    public ReservaFechaModel(){}

    public ReservaFechaModel(String fecha){
        this.fecha = fecha;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
