package com.techu.backend.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection="reservas")
public class ReservaModel {

    @Id
    @NotNull
    private String id;
    private String fecha;
    private String hora;
    private String origen;
    private String destino;
    private String matricula;
    private String nombre;

    public ReservaModel(){
    }

    public ReservaModel(String id, String fecha, String hora, String origen, String destino,
                        String matricula, String nombre){
        this.id = id;
        this.fecha = fecha;
        this.id = hora;
        this.fecha = origen;
        this.fecha = destino;
        this.fecha = matricula;
        this.nombre = nombre;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
