package com.techu.backend.model;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="vehiculo")
@JsonPropertyOrder({"id", "id"})
public class VehiculoModel {

    @Id
    private String id;
    @NotNull
    private String matricula;
    @NotNull
    private String marca;
    @NotNull
    private String modelo;
    private boolean reservado;
    private double carga;

    public VehiculoModel () {};
    public VehiculoModel(String id, @NotNull String matricula, @NotNull String marca, @NotNull String modelo) {
        this.id = id;
        this.matricula = matricula;
        this.marca = marca;
        this.modelo = modelo;
        this.reservado= false;
        this.carga=1.0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public boolean isReservado() {
        return reservado;
    }

    public void setReservado(boolean reservado) {
        this.reservado = reservado;
    }

    public double getCarga() {
        return carga;
    }

    public void setCarga(double carga) {
        this.carga = carga;
    }

    public boolean equals(VehiculoModel veh) {
        return  (this.getId() == veh.getId() && this.getCarga() == veh.getCarga() && this.getMatricula() == veh.getMatricula());
    }
}
