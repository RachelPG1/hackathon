package com.techu.backend.model;

public class VehiculoReservado {

    private String id;
    private boolean reservado;

    public VehiculoReservado() {}

    public VehiculoReservado(boolean reservado) {
        this.reservado = reservado;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setReservado(boolean reservado) {
        this.reservado = reservado;
    }

    public boolean isReservado() {
        return reservado;
    }
}
