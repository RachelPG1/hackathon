package com.techu.backend.service;

import com.techu.backend.model.ReservaModel;
import com.techu.backend.repository.ReservaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ReservaMongoService {

    @Autowired
    ReservaRepository reservaRepository;

    //READ
    // llamamos a este metodo igual que el de la clase padre de mongorepository
    public List<ReservaModel> findAll(){
        return reservaRepository.findAll();
    }

    // CREATE o SOBRESCRIBIR
    public ReservaModel save(ReservaModel newReserva){
        return reservaRepository.save(newReserva);
    }

    // READ por Id
    public Optional<ReservaModel> findById(String id){
        return reservaRepository.findById(id);
    }

    //Controla que exista
    public boolean existsById(String id){
        return reservaRepository.existsById(id);
    }

    // DELETE
    public boolean delete(ReservaModel reservaModel){
        //la excepcion controla que no haya error de escritura,no q este o no el reg
        try{
            reservaRepository.delete(reservaModel);
            return true;
        } catch (Exception ex){
            return false;
        }
    }

    // DELETE por ID
    public boolean deleteById(String id){
        //la excepcion controla que no haya error de escritura,no q este o no el reg
        try{
            reservaRepository.deleteById(id);
            return true;
        } catch (Exception ex){
            return false;
        }
    }
}
