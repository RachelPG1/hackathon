package com.techu.backend.service;

import com.techu.backend.model.VehiculoModel;
import com.techu.backend.repository.VehiculoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@Service
public class VehiculoService {

    private Logger LOGGER = Logger.getLogger("VehiculoController");
    private List<VehiculoModel> vehiculoModelList = new ArrayList<VehiculoModel>();

    @Autowired
    VehiculoRepository vehiculoRepository;

    public List<VehiculoModel> findAll() {
        return vehiculoRepository.findAll();
    }

    public Optional<VehiculoModel> findById (String id) { return vehiculoRepository.findById(id);}

    public String initVehiculos () {

        for (int i = 0; i < 10; i++) {

            vehiculoRepository.save(new VehiculoModel(Integer.toString(i), "000".concat(Integer.toString(i)).concat(" LDP"), "Toyota", "Yaris"));
        }

        return "Init OK";
    }

    public VehiculoModel save (VehiculoModel vehiculo) {
        return vehiculoRepository.save(vehiculo);

    }

    public void delete (VehiculoModel vehiculo) {
        vehiculoRepository.delete(vehiculo);
    }

    public void deleteById (String id) {
        vehiculoRepository.deleteById(id);
    }

    public VehiculoModel patchReserva (String id,boolean reserva){
        List<VehiculoModel> listVehiculos = vehiculoRepository.findAll();
        VehiculoModel vehiculoPath = null;

        for (VehiculoModel vehiculo: listVehiculos) {
            if (vehiculo.getId().equals(id)) {
                vehiculo.setReservado(reserva);
                vehiculoRepository.save(vehiculo);
                vehiculoPath = vehiculo;
                break;
            }
        }

        return vehiculoPath;

    }
}
