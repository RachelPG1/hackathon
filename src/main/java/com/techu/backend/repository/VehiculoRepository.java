package com.techu.backend.repository;

import com.techu.backend.model.VehiculoModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VehiculoRepository extends MongoRepository<VehiculoModel, String> {
}

