package com.techu.backend.controller;

import com.techu.backend.model.VehiculoModel;
import com.techu.backend.model.VehiculoReservado;
import com.techu.backend.service.VehiculoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;


@CrossOrigin
@RestController
@RequestMapping("${url.base}")
public class VehiculoController {

    private Logger LOGGER = Logger.getLogger("VehiculoController");

    @Autowired
    private VehiculoService vehiculoService;


    @GetMapping("")
    public static String index() {
        return "API HackathonV1.0";
    }


    @GetMapping("/init")
    public String init() {

        return vehiculoService.initVehiculos();
    }

    @GetMapping("/vehiculos")

    public List<VehiculoModel> getVehiculos() {
        LOGGER.log(Level.INFO,"getVehiculos");

        return vehiculoService.findAll();
    }

    @GetMapping("/vehiculos/{id}")
    public Optional<VehiculoModel> getVehiculoById(@PathVariable String id){
        LOGGER.log(Level.INFO,"VehiculoController - getVehiculoById");
        return vehiculoService.findById(id);
    }

    @PostMapping("/vehiculos")
    public VehiculoModel postVehiculo(@RequestBody VehiculoModel newVehiculo) {
        LOGGER.log(Level.INFO,"VehiculoController - postVehiculo");

        return vehiculoService.save(newVehiculo);
    }

    @PutMapping("/vehiculos")
    public void putProductos(@RequestBody VehiculoModel vehiculo){
        LOGGER.log(Level.INFO,"VehiculoController - putProductos");

        vehiculoService.save(vehiculo);
    }

    @DeleteMapping("/vehiculos")
    public void deleteVehiculo(@RequestBody VehiculoModel vehiculo){
        LOGGER.log(Level.INFO,"VehiculoController - deleteProductos");

        vehiculoService.delete(vehiculo);
    }

    @DeleteMapping("/vehiculos/{id}")
    public void  deleteVehiculoById(@PathVariable String id){
        LOGGER.log(Level.INFO,"VehiculoController - deleteProductos by Id");

        vehiculoService.deleteById(id);
    }

    @PatchMapping("/vehiculos/{id}")
    public ResponseEntity patchVehiculoReserva (@PathVariable String id, @RequestBody VehiculoReservado reserva) {
        VehiculoModel vh = vehiculoService.patchReserva (id,reserva.isReservado());

        if (null==vh){
            return new ResponseEntity<>("Se ha producido un error al reservar el vehiculo.", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(vh, HttpStatus.OK);


    }

}
