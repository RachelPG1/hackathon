package com.techu.backend.controller;

import com.techu.backend.model.ReservaModel;
import com.techu.backend.service.ReservaMongoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("${url.base}")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST,
RequestMethod.DELETE, RequestMethod.PUT})
public class ReservaController {

    @Autowired
    private ReservaMongoService reservaService;

    @GetMapping("")
    public String index(){
        return "API REST Hackathon Tech U! v1.0.0";
    }

   //GET a todas las reservas (Coleccion)
    @GetMapping("/reservas")
    public List<ReservaModel> getReservas(){
        return reservaService.findAll();
    }

    //GET a una reserva concreta por id (Instancia)
    @GetMapping("/reservas/{id}")
    public ResponseEntity getReservaById(@PathVariable String id){
        // Se puede comprobar si existe con un existById
        Optional<ReservaModel> pr = reservaService.findById(id);
        if (pr.isEmpty()) {
            return new ResponseEntity<>("Reserva no encontrada", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(pr, HttpStatus.OK);
    }

    //POST para crear una reserva
    @PostMapping("/reservas")
    public ReservaModel postReserva(@RequestBody ReservaModel newReserva){
        reservaService.save(newReserva);
        return newReserva;
    }

    @DeleteMapping("/reservas")
    public boolean deleteReserva(@RequestBody ReservaModel reservaToDelete){
        return reservaService.delete(reservaToDelete);
    }

    @DeleteMapping("/reservas/{id}")
    public ResponseEntity deleteReserva(@PathVariable String id) {
        Optional<ReservaModel> pr = reservaService.findById(id);
        if (pr.isEmpty()) {
            return new ResponseEntity<>("Reserva no encontrada", HttpStatus.NOT_FOUND);
        }
        reservaService.deleteById(id);
        return new ResponseEntity(pr, HttpStatus.OK);
        //return reservaService.delete(reservaToDelete);
    }

    @PutMapping("/reservas")
    public ReservaModel putReserva(@RequestBody ReservaModel reservaToUpdate){
        reservaService.save(reservaToUpdate);
        return reservaToUpdate;
    }

   /* @PatchMapping("/reservas/{id}")
    public boolean patchReserva(@PathVariable String id,
                                @RequestBody ReservaFechaModel fecha){

    }

    */
}
