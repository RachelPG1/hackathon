package com.hackathon.hackathon.controller;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class VehiculoControllerTests {

    @Test
    public void testIndex() {
        VehiculoController controller = new VehiculoController();
        assertEquals("API HackathonV1.0", VehiculoController.index());
    }

}
